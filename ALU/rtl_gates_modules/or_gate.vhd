library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity or_gate is
    Port ( i_bit0 : in  STD_LOGIC;    -- XOR gate input
           i_bit1 : in  STD_LOGIC;    -- XOR gate input
           o_bit0   : out STD_LOGIC);    -- XOR gate output
end or_gate;

architecture rtl of or_gate is
begin 
	
process(i_bit0,i_bit1)
begin
   if i_bit0 = '1' and i_bit1 = '1'  then o_bit0 <= transport '1' after 10 ns;		 
   elsif i_bit0 = '1' and i_bit1 = '0'  then o_bit0 <= transport '1' after 10 ns;
   elsif i_bit0 = '0' and i_bit1 = '1'  then o_bit0 <= transport '1' after 10 ns;	
   elsif i_bit0 = '0' and i_bit1 = '0'  then o_bit0 <= transport '0' after 10 ns;	
   else o_bit0 <= transport '0' after 10 ns; 
   end if;
	   
end process;
	   
end rtl;
