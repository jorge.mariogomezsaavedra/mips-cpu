library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity not_gate is
    Port ( i_bit0 : in  STD_LOGIC;    -- Not Gate input
           o_bit0   : out STD_LOGIC);   -- Not Gate output
end not_gate;

architecture rtl of not_gate is
begin 
	
process(i_bit0)
begin
   if    i_bit0 = '1' then o_bit0 <= transport '0' after 10 ns;		 
   elsif i_bit0 = '0' then o_bit0 <= transport '1' after 10 ns;
   else  o_bit0 <= transport 'U' after 10 ns;  
   end if;
	   
end process;
	   
end rtl;
