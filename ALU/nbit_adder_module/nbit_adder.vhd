library IEEE;	  

library rtl_gates_modules;
library half_adder_module;
library full_adder_module;

use rtl_gates_modules.all;
use half_adder_module.half_adder;
use full_adder_module.full_adder;

use IEEE.STD_LOGIC_1164.all;   


entity nbit_adder is  
	generic( N     :     integer);
    Port ( A  , B  : in  STD_LOGIC_VECTOR(N-1 downto 0);  -- A and B as inputs to n bit adder 
	Overflow,Negative,Zero       : out std_logic;
	Answer         : out STD_LOGIC_VECTOR(N-1 downto 0);
	CarryOut           : out std_logic
	);  

end nbit_adder;

architecture rtl of nbit_adder is

signal cin : std_logic_vector(N downto 0);
signal cout : std_logic_vector(N-1 downto 0) := (others => '0'); -- input values 	  
signal TestOne,TestTwo : std_logic := '0';
signal tmp : std_logic_vector(N-1 downto 0);

component xnor_gate
port( i_bit0 : in  STD_LOGIC;    -- XOR gate input
           i_bit1 : in  STD_LOGIC;    -- XOR gate input
           o_bit0   : out STD_LOGIC);    -- XOR gate output;
end component;

component full_adder
    port ( i_bit0  , i_bit1 , cin : in  STD_LOGIC;    -- XOR gate input
          sum , cout    : out STD_LOGIC);    -- XOR gate output
end component;

component xor_gate 	 
	port(i_bit0,i_bit1 : in std_logic;
	o_bit0 : out std_logic);	
end component;	 

component and_gate 	 
	port(i_bit0,i_bit1 : in std_logic;
	o_bit0 : out std_logic);	
	end component;
begin 	
-- port map of components to create a N bit adder

cin(0) <= '0';	-- No carry-in first bit

ALU: for I in 0 to N-1 generate  -- iterates through signal/FA & inputs/outputs
	Add0: full_adder port map ( A(I)  , B(I) , cin(I) , Answer(I) , cout(I));

        cin(I+1) <= cout(i);
           

ALU_last_bit: if I = N-1 generate
       AddN: full_adder port map(A(I),B(I),cin(I+1),Answer(I), cout(I)); 
          CarryOut <= cout(I);
       end generate;
end generate;	  



-- Negative test 
Negative <= tmp(N-1);

-- Overflow test
OflowTest0: xnor_gate port map(A(N-1), B(N-1), TestOne);	 -- checks the last bits if they are equal
OflowTest1: xor_gate  port map(B(N-1) , tmp(N-1), TestTwo);
OflowTest2: and_gate  port map(TestOne, TestTwo, Overflow); 

-- Zero 
Zero <= 'Z';



end rtl;
