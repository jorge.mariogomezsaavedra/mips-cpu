library ieee;
use ieee.std_logic_1164.all;

entity nbit_adder_tb is
end nbit_Adder_tb;

architecture tb of nbit_adder_tb is
  constant N  : integer := 3;
  signal A, B, Answer : std_logic_vector(N-1 downto 0):= "000";
  signal Overflow, Negative, Zero, CarryOut :  std_logic;
  

  
  component nbit_adder
  generic( N     :     integer :=3);
    port(
      A, B : in std_logic_vector(N-1 downto 0);
      Overflow, Negative, Zero  : out std_logic;
      Answer   : out std_logic_vector(N-1 downto 0);
      CarryOut : out std_logic
      );
  end component;

begin
  
  nbit:nbit_adder
  generic map(N => 3)
  port map(A,B, Overflow, Negative, Zero, Answer, CarryOut);


process
begin

  A <= "000";
  B <= "000";
  wait for 1000 ns;

  A <= "001";
  B <= "000";

  wait for 1000 ns;

  A <= "010";
  B <= "000";

  wait for 1000 ns;

  A <= "011";
  B <= "000";

  wait for 1000 ns;

  A <= "001";
  B <= "001";

  wait for 1000 ns;

  A <= "001";
  B <= "010";

  wait for 1000 ns;

  

  
end process ;
  
  
end tb;
