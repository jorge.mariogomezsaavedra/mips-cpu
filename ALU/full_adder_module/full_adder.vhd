library IEEE;

library rtl_gates_modules;
library half_adder_module;



use rtl_gates_modules.or_gate;
use half_adder_module.half_adder;
use IEEE.STD_LOGIC_1164.all;

entity full_adder is
    Port ( i_bit0  , i_bit1 , cin : in  STD_LOGIC;    -- XOR gate input
          sum , cout    : out STD_LOGIC);    -- XOR gate output
end full_adder;

architecture rtl of full_adder is

component or_gate 	 
	port(i_bit0,i_bit1 : in std_logic;
	o_bit0 : out std_logic);	
end component;	  

component half_adder 	 
	port(i_bit0 , i_bit1  : in std_logic;
	sum, cout : out std_logic);	
end component;	

signal t_sum, cout0,cout1 : std_logic := '0';

begin 	 

half_adder0: half_adder port map(i_bit0, i_bit1, t_sum , cout0);
half_adder1: half_adder port map(cin, t_sum, sum, cout1);
or_gate0: or_gate port map( cout0, cout1, cout); 
	


end rtl;
