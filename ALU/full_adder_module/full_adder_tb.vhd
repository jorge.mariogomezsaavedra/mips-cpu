library ieee;
use ieee.std_logic_1164.all;

entity full_adder_tb is
end full_adder_tb;

architecture tb of full_adder_tb is

  signal A,B,cin :  std_logic := '0';
  signal s,c :  std_logic :='0';
  
  component full_adder
    port(
      i_bit0, i_bit1, cin : in std_logic;
      sum, cout           : out std_logic);
  end component;

begin

 

  
  FA: full_adder
    port map(A,B,cin,s,c);
    
  process
  begin

    A <= '0';
    B <= '0';
    wait for 1000 ns;
    A <= '1';
    B <= '0';
    wait for 1000 ns;
    A <= '0';
    B <= '1';
    wait for 1000 ns;
    A  <= '1';
    B  <= '1';
    wait for 1000 ns;
  
  end process;



  end tb;
