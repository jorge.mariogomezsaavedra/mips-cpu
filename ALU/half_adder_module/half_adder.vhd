library IEEE;					  
--library mydesign;
--use mydesign.all;

library rtl_gates_modules;
use rtl_gates_modules.and_gate;
use rtl_gates_modules.xor_gate;

use IEEE.STD_LOGIC_1164.all;  


entity half_adder is	

    Port ( i_bit0   , i_bit1      : in  std_logic;    -- XOR gate input
           sum , cout   : out std_logic);    -- XOR gate output
end half_adder;

architecture rtl of half_adder is		  

component xor_gate 	 
	port(i_bit0,i_bit1 : in std_logic;
	o_bit0 : out std_logic);	
end component;	 

component and_gate 	 
	port(i_bit0,i_bit1 : in std_logic;
	o_bit0 : out std_logic);	
	end component;
	
begin 	 

	
AND0: and_gate port map ( i_bit0 , i_bit1 , cout );
XOR0: xor_gate port map ( i_bit0 , i_bit1 , Sum  );
	   
end rtl;
