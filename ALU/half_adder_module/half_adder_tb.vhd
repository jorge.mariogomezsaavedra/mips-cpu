library ieee;
use ieee.std_logic_1164.all;

entity half_adder_tb is
end half_adder_tb;

architecture tb of half_adder_tb is
  signal r_bit0 : std_logic := '0';
  signal r_bit1 : std_logic := '0';
  signal w_sum  : std_logic;
  signal w_carry: std_logic;
  component half_adder
    port(i_bit0,i_bit1 : in std_logic; sum,cout : out std_logic);
  end component;

begin
  
  HA :  half_adder
    port map(r_bit0,r_bit1,w_sum,w_carry);
    
  
  process
  begin
  
    r_bit0 <= '0';
    r_bit1 <= '0';
    wait for 1000 ns;
    r_bit0 <= '1';
    r_bit1 <= '0';
    wait for 1000 ns;
    r_bit0 <= '0';
    r_bit1 <= '1';
    wait for 1000 ns;
    r_bit0 <= '1';
    r_bit1 <= '1';
    wait for 1000 ns;
    
  end process;

  end tb; 
